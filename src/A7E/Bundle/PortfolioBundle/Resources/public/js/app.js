(function ($) {
    'use strict';

    var app = {
        init: function () {
            console.log('application initialisée ...');
            $('.parallax').parallax();
            // Initialize collapse button
            $(".button-collapse").sideNav();

            $('input#input_name, input#input_email, textarea#input_message').characterCounter();

            var options = [
                {selector: '.col-left-anim', offset: 200, callback: 'app.animIn(".col-left-anim", "bounceInLeft")' },
                {selector: '.col-right-anim', offset: 200, callback: 'app.animIn(".col-right-anim", "bounceInRight")' },
                // {selector: '#skills', offset: 350, callback: 'app.animIn("#skills, #currentStatus", "fadeIn")' },
                // {selector: '#currentStatus', offset: 200, callback: 'app.animIn("#currentStatus", "fadeIn")' },
                {selector: '#careerTimeline', offset: 350, callback: 'Materialize.showStaggeredList("#careerTimeline")' },
                {selector: '#contactFormCard', offset: 200, callback: 'app.animIn("#contactFormCard", "bounceInUp")' }
            ];
            Materialize.scrollFire(options);

            this.scrollButton();
        },

        animIn: function (el, animation) {
            var $el = $(el);
            console.log(el);
            $el.addClass('animated ' + animation);
            $el.removeClass('invisible-opacity');
        },

        scrollButton: function () {
            $('#scrollBtn').click( function() { // Au clic sur un élément
    			var page = $(this).data('target'); // Page cible
    			var speed = 400; // Durée de l'animation (en ms)
    			$('html, body').animate( { scrollTop: $(page).offset().top }, speed ); // Go
    			return false;
    		});
        }

    };

    window.app = app;
}(jQuery));
