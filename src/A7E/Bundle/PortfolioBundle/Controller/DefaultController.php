<?php

namespace A7E\Bundle\PortfolioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $params = array();
        return $this->render('A7EPortfolioBundle:Default:index.html.twig', $params);
    }

    public function projectAction($id)
    {
        
        $params = array(
            'id' => $id
        );

        return $this->render('A7EPortfolioBundle:Default:project.html.twig', $params);
    }
}
